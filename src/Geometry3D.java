public class Geometry3D {
    public static class Cuboid {
        private double length;
        private double breadth;
        private double height;


        public Cuboid(double length, double breadth, double height) {
            this.length = length;
            this.breadth = breadth;
            this.height = height;
        }

        public double volume() {
            return length * breadth * height;
        }

        public double surfaceArea() {
            return 2 * (length * breadth + length * height + breadth * height);
        }




    }

    public static class SquareBasedPyramid {
        private double base;
        private double height;

        public SquareBasedPyramid(double base, double height) {
            this.base = base;
            this.height = height;
        }

        public double volume() {
            return 1.0 / 3.0 * base * base * height;
        }
    }

    public static double squareBasedPyramidSurfaceArea(double base, double height) {
        double slantHeight = Math.sqrt(height * height + base * base / 4.0);
        return base * base + base * slantHeight * 2.0;
    }

    public static class Tetrahedron {
        private double sideLength;

        public Tetrahedron(double sideLength) {
            this.sideLength = sideLength;
        }

        public double volume() {
            return (Math.pow(sideLength, 3) / (6 * Math.sqrt(2)));
        }
        public double surfaceArea() {
            return Math.sqrt(3) * sideLength * sideLength;
        }
    }


}
