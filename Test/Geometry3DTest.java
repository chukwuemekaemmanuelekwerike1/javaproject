import org.junit.Test;
import static org.junit.Assert.*;

public class Geometry3DTest {
    @Test
    public void testCuboidVolume() {
        Geometry3D.Cuboid cuboid = new Geometry3D.Cuboid(3, 4, 5);
        double expectedVolume = 60.0;
        double actualVolume = cuboid.volume();
        assertEquals(expectedVolume, actualVolume, 0.0001);
    }
    @Test
    public void testCuboidSurfaceArea() {
        Geometry3D.Cuboid cuboid = new Geometry3D.Cuboid(3, 4, 5);
        double expectedSurfaceArea = 94.0;
        double actualSurfaceArea = cuboid.surfaceArea();
        assertEquals(expectedSurfaceArea, actualSurfaceArea, 0.0001);
    }
    @Test
    public void testSquareBasedPyramidVolume() {
        Geometry3D.SquareBasedPyramid pyramid = new Geometry3D.SquareBasedPyramid(5, 3);
        double expectedVolume = 24.999999999999996;
        double actualVolume = pyramid.volume();
        assertEquals(expectedVolume, actualVolume, 0.0001);
    }
    @Test
    public void testSquareBasedPyramidSurfaceArea() {
        double base = 5.0;
        double height = 3.0;
        double expectedSurfaceArea = 64.05124837953326;
        double delta = 0.001;

        double result = Geometry3D.squareBasedPyramidSurfaceArea(base, height);

        assertEquals(expectedSurfaceArea, result, delta);
    }

    @Test
    public void testTetrahedronVolume() {
        Geometry3D.Tetrahedron t1 = new Geometry3D.Tetrahedron(2.0);
        assertEquals(0.9428090415820632, t1.volume(), 0.001);

        Geometry3D.Tetrahedron t2 = new Geometry3D.Tetrahedron(5.0);
        assertEquals(14.73, t2.volume(), 0.01);

        Geometry3D.Tetrahedron t3 = new Geometry3D.Tetrahedron(10.0);
        assertEquals(117.85, t3.volume(), 0.01);
    }
    @Test
    public void testSurfaceArea() {
        Geometry3D.Tetrahedron tetrahedron = new Geometry3D.Tetrahedron(5);
        assertEquals(43.301270189221924, tetrahedron.surfaceArea(), 0.01);
    }
}
